class Donation < ActiveRecord::Base
  belongs_to :donor

  def self.donation_sum
    Donation.sum('amount')
  end
end