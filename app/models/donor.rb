class Donor < ActiveRecord::Base
  has_many :donations

  def donor_total
    self.donations.sum('amount')
  end

end
