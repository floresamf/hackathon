class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.integer :donor_id
      t.float :amount
      t.datetime :date

      t.timestamps
    end
  end
end
